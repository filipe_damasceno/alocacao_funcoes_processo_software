'''
Created on 27 de mar de 2016

@author: Filipe Damasceno
'''
bancoName = 'recursos.db'
import sqlite3 as sql
import os.path

class Facade():
    
    @staticmethod
    def criar_tables():
        con = sql.connect(bancoName)
        cursor = con.cursor()
        '''tabela de agentes'''
        
        cursor.execute(""" 
            CREATE TABLE Agentes (
                id INTEGER NOT NULL PRIMARY KEY,
                nome TEXT NOT NULL
            );
        """)
        
        ''' tabela de papeis'''
        
        cursor.execute(""" 
            CREATE TABLE Papeis (
                idpapel INTEGER NOT NULL,
                idAgente INTEGER NOT NULL,
                nome TEXT NOT NULL,
                producao REAL NOT NULL,
                experiencia REAL NOT NULL,
                valor REAL NOT NULL,
                FOREIGN KEY (idAgente) REFERENCES Agentes(id),
                PRIMARY KEY (idpapel, idAgente)
            );
        """)
        
        cursor.execute(""" 
            CREATE TABLE Afinidades (
                idAgente INTEGER NOT NULL,
                idOutro INTEGER NOT NULL,
                afinidade REAL NOT NULL,
                FOREIGN KEY (idAgente) REFERENCES Agentes(id),
                FOREIGN KEY (idOutro) REFERENCES Agentes(id),
                PRIMARY KEY (idOutro, idAgente)
            );
        """)
        con.close()
        print("tabelas criadas com sucesso!!!!!!")
    
    @staticmethod
    def getAfinidades(agente,outro):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor()
            cursor.execute("""
            SELECT afinidade FROM Afinidades WHERE idAgente = ? AND idOutro = ?
            """,(agente, outro))
            valor = cursor.fetchall()
        db.close()
        return valor[0][0]
    
    @staticmethod       
    def getPapeis(idagente,idpapel):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor()
            cursor.execute("""
            SELECT producao, experiencia, valor FROM Papeis WHERE idpapel = ? AND idAgente = ?
            """,(idpapel,idagente))
            valor = cursor.fetchall()
        db.close()
        return valor[0]
    
    @staticmethod
    def getPreco(idagente,idpapel):
        return Facade.getPapeis(idagente, idpapel)[2]
    
    @staticmethod
    def getExperiencia(idagente,idpapel):
        return Facade.getPapeis(idagente, idpapel)[1]
    
    @staticmethod
    def getProducao(idagente,idpapel):
        return Facade.getPapeis(idagente, idpapel)[0]
    
    @staticmethod
    def getIdsAgente():
        var = []
        
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor()
            cursor.execute("""
                SELECT id FROM Agentes;
            """)
            var = cursor.fetchall()
        db.close()
        return [i[0] for i in var]
    
    @staticmethod
    def getAgentes():
        var = []
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor()
            cursor.execute("""
                SELECT * FROM Agentes;
            """)
            var = cursor.fetchall()
        db.close()
        return var
    
    @staticmethod
    def getNomeAgentes(ida):
        var = []
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor()
            cursor.execute("""
                SELECT nome FROM Agentes WHERE id = ?;
            """,(ida,))
            var = cursor.fetchall()
        db.close()
        return var[0][0]
    
    @staticmethod
    def add_Agente(id,nome):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor()
            cursor.execute("""
            INSERT INTO Agentes (id, nome) VALUES (?,?)
            """,(id, nome))
            db.commit()
        db.close()
        
    @staticmethod
    def add_Papel(idagente,idpapel,nome,produtividade,experiencia,valor):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor() 
            cursor.execute("""
            INSERT INTO Papeis (idpapel, idAgente, nome, producao, experiencia, valor) VALUES (?,?,?,?,?,?)
            """,(idpapel, idagente, nome, produtividade, experiencia, valor))
            db.commit()
        db.close()
    
    @staticmethod      
    def add_Afinidade(ag1,ag2,afinidade):
        BASE_DIR = os.path.dirname(os.path.abspath(__file__))
        db_path = os.path.join(BASE_DIR, bancoName)
        with sql.connect(db_path) as db:
            cursor = db.cursor() 
            cursor.execute("""
            INSERT INTO Afinidades (idAgente, idOutro, afinidade) VALUES (?,?,?)
            """,(ag1,ag2,afinidade))
            db.commit()
        db.close()
    
    @staticmethod       
    def add_dadosRand():
        Facade.add_agentes()
        print("nomes adicionados!!!!!!!")
        Facade.add_papeis()
        print("papeis adicionados!!!!!!!")
        Facade.add_Afinidades()
        print("afinidades adicionados!!!!!!!")
    
    @staticmethod    
    def add_Afinidades():
        import random
        lista = Facade.getIdsAgente()
        for i in lista:
            for j in lista:
                if i != j:
                    Facade.add_Afinidade(i[0], j[0], random.random())                
    
    @staticmethod
    def add_papeis():
        import random 
        nomes = ["Gerente","Analista de requisitos","Arquiteto(a) de software","Programador(a)","Testador(a)"]
        
        for j in Facade.getIdsAgente():
            for i,nome in enumerate(nomes):
                prod = random.random()
                exp = random.random()
                val = random.randint(30,200)
                Facade.add_Papel(j[0], i, nome, prod, exp, val)
    
    @staticmethod
    def add_agentes():
        file = open('nomes','r')
        names = [i[:-1] for i in file.readlines()] 
        for i,nome in enumerate(names):
            Facade.add_Agente(i, nome)
            
"""def decodificador(codificacao=[1,2,3,4,5]):
    for i in range(1,len(codificacao)):
        codificacao[i]+=codificacao[i-1]    
    vtac = []
    for i in range(len(codificacao)):
        if i==0:
            vtac.append(range(0,codificacao[i]))
        else:
            vtac.append(range(codificacao[i-1],codificacao[i]))
    return vtac

papeis = decodificador()

def getPapel(pos=0):
        for i in range(len(papeis)):
            if pos in papeis[i]:
                return i
acc = 0
for p,i in enumerate([40, 3, 18, 10, 14, 16, 2, 39, 24, 42, 27, 28, 44, 12, 49]):
    acc += Facade.getExperiencia(i, getPapel(p))
print(acc/15)"""