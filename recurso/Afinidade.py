'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''

class Afinidade(object):
    '''
    grada a afinidade do individuo atual com outros
    '''

    def __init__(self, nivel = 0.0, outro=None):
        '''
        Constructor
        '''
        self.__nivel = nivel
        self.__outro = outro
    
    def getnivel(self):
        return self.__nivel
    def setnivel(self, nivel=0.0):
        if 0.0 <= nivel <= 1:  
            self.__nivel = nivel
    nivel = property(fget=getnivel, fset=setnivel)
    
    def getoutro(self):
        return self.__outro
    outro = property(fget = getoutro)
    
    
        