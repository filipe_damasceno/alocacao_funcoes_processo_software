'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''
from recursos.Afinidade import Afinidade
from recursos.Papel import Papel

class Agente(object):
    '''
    o agente do processo
    '''
    ids=[]
    
    def __init__(self, id=0, nome=""):
        '''
        contem apenas um __nome e as classes de papel e afinidade
        '''
        self.__id=id
        Agente.ids.append(id)
        self.__nome = nome
        self.__papeis=[]
        self.__afinidades=[]
    
    def getid(self):
        return self.__id
    id = property(fget = getid)
    
    def getnome(self):
        return self.__nome
    def setnome(self, nome=""):
        if len(nome) > 3:
            self.__nome = nome
    nome = property(fget = getnome, fset = setnome)
    
    def add_papel(self,experiencia=0.0,preco=0.0,produtividade=0.0,nome=0):
        if experiencia or preco or produtividade: return
        
        flag = False
        for i in self.__papeis:
            if i.codigocargo == nome:
                flag =True
        if not flag:
            self.__papeis.append(Papel(nome,experiencia,preco,produtividade))
            
    def buscarPapel(self,cod=0):
        for i in self.__papeis:
            if cod == i.codigocargo:
                return i
    
    def add_afinidade(self,nivel=0.0, outro=None):
        if outro == None: return
        flag = False
        for i in self.__afinidades:
            if i.outro == outro:
                flag = True
        if flag:
            self.__afinidades.append(Afinidade(nivel,outro))
            
    def buscar_afinidade(self,id=0):
        if id == self.id: return 0
        for i in self.__afinidades:
            if id == i.outro.id:
                return i.nivel
        return 0           
        