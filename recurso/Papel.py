'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''

'''
gerente: 1
analista de requisitos: 2
arquiteto de software: 3
programador: 4
testador: 5
'''

class Papel(object):
    '''
    contem as habilidades de cada um bem definida com as definicoes:
    '''


    def __init__(self, codigocargo=0,experiencia=0.0,preco=0.0,produtividade=0.0):
        '''
        gerente: 1
        analista de requisitos: 2
        arquiteto de software: 3
        programador: 4
        testador: 5
        '''
        self.__codigocargo = codigocargo
        self.__experiencia = experiencia
        self.__preco = preco
        self.__produtividade = produtividade
        
        def getcodigocargo(self):
            return self.__codigocargo
        def setcodigocargo(self, codigo=0):
            if 1 < codigo < 6:
                self.__codigocargo = codigo
        codigocargo = property(fget = getcodigocargo,fset = setcodigocargo)
        
        def getexperiencia(self):
            return self.__experiencia
        def setexperiencia(self, experiencia=0.0):
            if 0.0<=experiencia<=1:
                self.__experiencia = experiencia
        experiencia = property(fget = getexperiencia,fset = setexperiencia)
        
        def getpreco(self):
            return self.__preco
        def setpreco(self, valor=0.0):
            if 0.0<=valor:
                self.__preco = valor
        preco = property(fget = getpreco,fset = setpreco)
        
        def getprodutividade(self):
            return self.__produtividade
        def setprodutividade(self, prod=0.0):
            if 0.0<=prod<=1:
                self.__produtividade = prod
        produtividade = property(fget = getprodutividade,fset = setprodutividade)