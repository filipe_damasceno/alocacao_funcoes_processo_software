'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''
from individuo.Cromossomo import Cromossomo

class Populacao(object):
    '''
    nessa classe estarao um conjunto de individuos
    '''


    def __init__(self,cromossomos=[]):
        '''
        havera um numero grande de individuos, de sorte que tem que adicionar muitos individuos.
        '''
        self.__cromossomos = cromossomos
        self.__melhorIndividuo = None
        
        self.desvio = []
        self.media = []
        self.melhor = []
        self.pior = []
    
    @property
    def cromossomos(self):
        return self.__cromossomos
    @cromossomos.setter
    def cromossomos(self,c):
        if type(c) == type([]):
            self.__cromossomos = c
    @cromossomos.deleter
    def cromossomos(self):
        del self.__cromossomos
    
    @property
    def melhorIndividuo(self):
        return self.__melhorIndividuo
    @melhorIndividuo.setter
    def melhorIndividuo(self,m):
        if type(m) == type(Cromossomo()):
            self.__melhorIndividuo = m
    @melhorIndividuo.deleter
    def melhorIndividuo(self):
        del self.__melhorIndividuo
    
    def salvar(self):
        self.__cromossomos.sort(key = lambda objeto: objeto.fitness,reverse = True)
        self.desvio.append(self.desviop())
        self.media.append(self.calcMedia())
        self.melhor.append(self.__cromossomos[0].fitness)
        self.pior.append(self.__cromossomos[-1].fitness)
        
        if self.__melhorIndividuo == None:
            self.__melhorIndividuo = self.__cromossomos[0]
        
        if self.__cromossomos[0].fitness > self.__melhorIndividuo.fitness:
            self.__melhorIndividuo = self.__cromossomos[0].copy()
    
    def desviop(self):
        val=0
        for i in self:
            val += i.fitness
        media = val/len(self)
        sun = 0
        for v in self:
            valor = v.fitness
            sun += (valor - media)**2
        varianca = sun/len(self)
        return varianca**(0.5)
    
    def calcMedia(self):
        '''
        private
        '''
        acc=0
        for i in self:
            acc += i.fitness
        return acc/len(self) 
    
    """
    -----------sobrecargas----------------
    """
    
    def __len__(self):
        return len(self.__cromossomos)
    
    def __getitem__(self, id):
        return self.__cromossomos[id]
    
    def __setitem__(self, id,valor):
        if type(valor) == type(self[0]):
            self.__cromossomos[id] = valor
    def __str__(self, *args, **kwargs):
        v=""
        for i in self:
            v+= str(i)+"\n"
        return v