'''
Created on 4 de fev de 2016

@author: FACOMP
'''
import matplotlib.pyplot as ptl

def plotar(melhor,medias,pior,desvio, ngeracoes = 0):
    superior = [d+m for d,m in zip(desvio,medias)]
    inferior = [m-d for d,m in zip(desvio,medias)]
    ngeracoes = range(len(melhor)) if ngeracoes == 0 else range(ngeracoes)
    ptl.plot(ngeracoes,melhor,'b-',label="melhor")
    ptl.plot(ngeracoes,medias,'y-',label="medias")
    ptl.plot(ngeracoes,pior,'r-',label="pior")
    ptl.plot(ngeracoes,superior,'g--',label="desvio")
    ptl.plot(ngeracoes,inferior,'g--')
    ptl.xlabel("Geracoes")
    ptl.ylabel("aproximacao")
    ptl.legend(bbox_to_anchor=(0., 1.02, 1., .102), loc=3,
           ncol=2, mode="expand", borderaxespad=0.)
    ptl.show()
