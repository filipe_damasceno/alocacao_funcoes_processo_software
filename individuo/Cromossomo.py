'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''
import random
from copy import deepcopy as cpy


class Cromossomo(object):
    '''
    esta classe contem um conjunto de genes
    '''

    def __init__(self, tamanho=0, ids=[],codificacao=[]):
        '''
        gera um cromossomo contendo os genes es funcoes atribuidas a ela.
        codificacao possui uma lista para definir a codificacao
        '''     
        self.codificado = codificacao
        self.cod_papeis = None
        self.__decodificador(codificacao)
        self.ids = ids
        self.__genes = random.sample(self.ids,tamanho)
        self.__fAfinidade = 0
        self.__fPreco = 0
        self.__fProdutividade = 0
        self.__fExperiencia = 0
        self.__fitness = 0
    
    def __decodificador(self,codificacao):
        for i in range(1,len(codificacao)):
            codificacao[i]+=codificacao[i-1]
            
        vtac = []
        for i in range(len(codificacao)):
            if i==0:
                vtac.append(range(0,codificacao[i]))
            else:
                vtac.append(range(codificacao[i-1],codificacao[i]))
        self.cod_papeis = vtac
    
    '''
    +=+=+=+=+=+=+=+=+=+=+=+=+get,set+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    '''
    @property
    def genes(self):
        return cpy(self.__genes)
    @genes.setter
    def genes(self, genes):
        if genes.__class__ == list and genes[1].__class__ == int:
            self.__genes = cpy(genes)
    @genes.deleter
    def genes(self):
        del self.__genes

    @property
    def fitness(self):
        return self.__fitness

    '''
    +=+=+=+=+=+=+=+=+=+=+=+=+func fitness+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    '''
    def calcfitness(self,pafinidade=1,ppreco=1,pprodutividade=1,pexperiencia=1,recursos=[]):
        self.__fitnessafinidade(recursos)
        self.__fitnessExperiencia(recursos)
        self.__fitnessPreco(recursos)
        self.__fitnessProdutividade(recursos)
        
        self.__fitness= self.__fAfinidade*pafinidade+\
                      self.__fPreco*ppreco+\
                      self.__fProdutividade*pprodutividade+\
                      self.__fExperiencia*pexperiencia
    
    def __fitnessafinidade(self,recursos=[]):
        media_afinidade=0 
        for i in self:
            for j in self:
                if i != j:
                    media_afinidade += recursos.getAfinidades(i,j)
        self.__fAfinidade = media_afinidade/(len(self)*(len(self)-1))
    
    def __fitnessPreco(self,recursos=None):
        media_precos=0
        for i in range(len(self)):    
            media_precos += recursos.getPreco(self[i],self.__getPapel(i))#agente.buscarPapel(self.__getPapel(i)).preco
        media_precos/= len(self)
        self.__fPreco = 1/media_precos
    
    def __fitnessExperiencia(self, recursos=None):        
        media_experiencia=0
        for i in range(len(self)):    
            media_experiencia += recursos.getExperiencia(self[i], self.__getPapel(i))
        media_experiencia = media_experiencia/len(self)
        self.__fExperiencia = media_experiencia
    
    def __fitnessProdutividade(self, recursos=[]):
        media_produtividade=0
        for i in range(len(self)):    
            media_produtividade += recursos.getProducao(self[i],self.__getPapel(i))
        media_produtividade/= len(self)
        self.__fProdutividade = media_produtividade
        
    def __getPapel(self,pos=0):
        for i in range(len(self.cod_papeis)):
            if pos in self.cod_papeis[i]:
                return i
    
    def mutacao(self,taxa=0.01):       
        for i in range(len(self)):
            if random.random() < taxa:
                self[i] = self.new_individuo(self.genes, self[i])
    
    def new_individuo(self,genes,individuo_atual=0):
        import random as rand
        novo = None #valor que nao esta nos ids 
        novo = individuo_atual
        while novo in genes:
            randVal = rand.random()
            moeda = 1 if rand.random() > 0.5 else 0 #1 = cara casasAcima, 0 = coroa casasAbaixo
            if randVal <= 0.5:
                novo+= 1 if moeda else -1
            elif randVal <= 0.8:
                novo+= 2 if moeda else -2
            elif randVal <= 0.95:
                novo+= 3 if moeda else -3
            else:
                novo= random.choice(genes)
            
            if novo > self.ids[-1]:
                novo = self.ids[-1] - (novo - self.ids[-1])
            elif novo < self.ids[0]:
                novo = novo * (-1)
        return novo
    '''
    +=+=+=+=+=+=+=+=+=+=+=+=+sobrecarga+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+=+
    '''
    def decode(self,recursos):
        a = [recursos.getNomeAgentes(i) for i in self.genes]
        return a.__str__()+" ___ "+(1/self.__fPreco).__str__()
            
    def copy(self):
        """v = self.__class__(len(self), self.ids, self.codificado)
        v.genes = cpy(self.genes)"""
        return cpy(self)
        
    def __getitem__(self,index=-1): # Cromossomo[index]
        return self.__genes[index]
    
    def __setitem__(self,index,valor): # Cromossomo[index]
        if valor in self.ids:
            self.__genes[index] = valor
    
    def __len__(self):
        return len(self.__genes)
    
    def __add__(self,direita):#crossOver 
        '''
        realiza CrossOver, de modo a facilitar o processo 
        foi sobrecarregado o somador.
        '''
        posicao = random.randint(0,len(self))
        def copyset(pai,posicao,filho):
            acc =[]
            cont= len(self)-posicao
            for i in pai[posicao:]+pai[:posicao]:
                if i in filho:
                    continue
                else:
                    cont-=1
                    acc.append(i)
                    if cont==0:
                        break
            return acc

        
        filho1 = self[:posicao]
        filho2 = direita[:posicao]
        filho1+= copyset(direita, posicao, filho1)
        filho2+= copyset(self, posicao, filho2)
        
        f1 = self.__class__(len(self),self.ids,self.codificado)
        f2 = self.__class__(len(self),self.ids,self.codificado)
        f1.genes = cpy(filho1)
        f2.genes = cpy(filho2)
        
        del filho1,filho2
        
        return [f1,f2] 
    
    def __str__(self):
        
        return "cromossomo: "+str(self.genes)+"fitness:\n"+\
            "Afinidade: "+str(self.__fAfinidade)+\
            "\nPreco: "+str(self.__fPreco)+\
            "\nProdutividade: "+str(self.__fProdutividade)+\
            "\nExperiencia: "+str(self.__fExperiencia)+\
            "\nFitness Total: "+str(self.__fitness)