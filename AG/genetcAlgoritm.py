'''
Created on 12 de mar de 2016

@author: Filipe Damasceno
'''
from individuo.Cromossomo import Cromossomo
import random
from recurso.DBconector import Facade
from Populacao.populacao import Populacao
from Graficos.Plotar import plotar


ngerentes=1
nAnalistas_req=2
nArquitetos_soft=3
nProgramadores=4
nTestadores=5

Codificado = [ngerentes, nAnalistas_req, nArquitetos_soft, nProgramadores, nTestadores]

Peso_afinidade=1
Peso_preco=1
Peso_produtividade=1
Peso_experiencia=1


Ring = 4
Lambda = 4 #deve ser par.
Taxa_CrossOver = 0.8
Taxa_mutacao = 0.02
TamanhoPop = 30
TamanhoCromossomo = sum([ngerentes,nAnalistas_req,nArquitetos_soft,nProgramadores,nTestadores])

def gera_populacao(tamanho = TamanhoPop,tamanhoCromossomo = TamanhoCromossomo,codificacao = Codificado):
    ids = Facade.getIdsAgente()
    vet_pop = []
    for i in range(tamanho):
        vet_pop.append(Cromossomo(tamanhoCromossomo, ids, codificacao))
    
    return Populacao(vet_pop)

def calcFitness(indis=[],pafinidade  = Peso_afinidade, ppreco = Peso_preco,\
                 pprodutividade = Peso_produtividade, pexperiencia = Peso_experiencia, Recursos=Facade):
    '''
    funcao calcfitness OK
    '''
    for i in range(len(indis)):
        indis[i].calcfitness(pafinidade,ppreco,pprodutividade,pexperiencia,Recursos)


def seleciona_pais(populacao=None,numeroPais=Lambda):
    '''
    selecionara o numero de pais Lambda
    '''
    vetor_pais = []
    for i in range(numeroPais//2):
        vetor_pais.append((torneio(populacao),torneio(populacao)))
    return vetor_pais

def torneio(populacao=None):
    '''
    neste torne e retornado apartir de uma disputa/
    o melhor individuo
    '''
    if populacao == None: return
        
    ring = []
    for i in range(Ring):
        selecionado = random.choice(populacao)
        while selecionado in ring:
            selecionado = random.choice(populacao)
        ring.append(selecionado)
        
    return max(ring,key = lambda object: object.fitness).copy()

def crossOver(pais_selecionados=[],taxa_cossover=Taxa_CrossOver):
    '''
    eh verificada a possibilidade de ocorrer o crizamento, 
    caso ocorra o operador sobrcarregado + (soma).
    '''
    filhos = []
    for pai1,pai2 in pais_selecionados:
        if random.random() <= taxa_cossover:
            filhos += pai1+pai2
        else:
            filhos.append(pai1)
            filhos.append(pai2)
    return filhos

def mutation(populacao = [],taxa_mutacao=0): 
    '''
    Mutacao com sigma e gaussiana
    funcao mutacao OK - no individuo
    ''' 
    for i in populacao:
        i.mutacao(taxa_mutacao)

def selection(pop = None,filhos=[],Lambda=0):
    #filhos.sort(key = lambda object: object.fitness, reverse = True)
    for i in range(Lambda):
        pop[(len(pop)-Lambda)+i] = filhos[i]
        
def ag():
    anterior = None
    contador = 0
    geracaoatual = 0
    
    def mudanca(cromossomo):
        if cromossomo == anterior:
            contador +=1
        else:
            anterior = cromossomo
            contador = 0
    populacao = gera_populacao(TamanhoPop, TamanhoCromossomo, Codificado)
    calcFitness(populacao)
    
    while contador <= 20 and geracaoatual < 200:
        populacao.salvar()
        pais = seleciona_pais(populacao, Lambda)
        filhos = crossOver(pais, Taxa_CrossOver)
        mutation(filhos, Taxa_mutacao)
        calcFitness(filhos)
        selection(populacao, filhos, Lambda)
        geracaoatual+=1
        print(geracaoatual)
        del filhos,pais
    populacao.salvar()
    print("melhor individuo decodificado:\n")
    print(populacao.melhorIndividuo.decode(Facade))
    
    return populacao.media,populacao.desvio,populacao.melhor,populacao.pior,populacao.melhorIndividuo


def resultado():
    resultados = []
    menor=10000
    melhor = 0
    melhorInd = None
    ntestes = 5
    for i in range(ntestes):
        resultados.append(ag())
        
        if len(resultados[-1][0]) < menor:
            menor = len(resultados[-1][0])
        
        if resultados[-1][4].fitness > melhor:
            melhorInd = resultados[-1][4]
            melhor = resultados[-1][4].fitness

    media = [0 for i in range(menor)]
    desvio = [0 for i in range(menor)]
    melhor = [0 for i in range(menor)]
    pior = [0 for i in range(menor)]
    
    for i in resultados:
        for j,a in enumerate(i[0]):
            if j < menor: 
                media[j] += a
        
        for j,a in enumerate(i[1]):
            if j < menor: 
                desvio[j] += a
        
        for j,a in enumerate(i[2]):
            if j < menor: 
                melhor[j] += a
        
        for j,a in enumerate(i[3]):
            if j < menor: 
                pior[j] += a
         
    for i in range(menor):
        media[i] /= ntestes
        desvio[i] /= ntestes
        melhor[i] /= ntestes
        pior[i] /= ntestes
    
    print(melhorInd)
    print(melhorInd.decode(Facade))
    
    plotar(melhor, media, pior, desvio, menor)

resultado()